from django.conf import settings


def get_sasl_auth():
    sasl_auth = getattr(settings, "KAFKA_SASL_AUTH", None)

    if isinstance(sasl_auth, dict):
        return sasl_auth

    if sasl_auth and isinstance(sasl_auth, str):
        sasl_user, sasl_pass = sasl_auth.split()
        return {
            'security_protocol': 'SASL_PLAINTEXT',
            'sasl_mechanism': 'SCRAM-SHA-512',
            'sasl_plain_username': sasl_user,
            'sasl_plain_password': sasl_pass
        }

    return {}
