from __future__ import annotations

import asyncio
import dataclasses
import logging
import os
import signal
import time
import uuid
from concurrent.futures import ThreadPoolExecutor
from enum import auto, Enum
from typing import Any, Callable, Coroutine, Dict, List, Optional, Type

import aiokafka
import django
from asgiref.sync import SyncToAsync
from django import db
from django.conf import settings
from django.db.models import Model
from django.utils.module_loading import import_string

from django_kafka.auth import get_sasl_auth

logger = logging.getLogger(__name__)

LEN_PROGRESS = 30
DEFAULT_BATCH_SIZE = 1000
COUNT_LOG_MESSAGE_BETWEEN_BATCH = 2
LOG_EVERY_RECORD = int(DEFAULT_BATCH_SIZE / COUNT_LOG_MESSAGE_BETWEEN_BATCH)


class KafkaConsumer(aiokafka.AIOKafkaConsumer):
    async def wait_for_assignment(self):
        while not self.assignment():
            self._coordinator.check_errors()
            await self._subscription.wait_for_assignment()


class HandlerResult(Enum):
    Saved = auto()
    Fail = auto()


class ConsumerTimeoutError(Exception):
    pass


def _bulk_create(cls: Model, objs: list[Model], batch_size: int):
    cls.objects.bulk_create(objs, batch_size=batch_size, ignore_conflicts=True)


def uuid_deserializer(value: bytes) -> uuid.UUID:
    if len(value) == 16:
        return uuid.UUID(bytes=value)
    return uuid.UUID(value.decode())


async def consumer_routine(
    queues: Dict[str, asyncio.Queue],
    consumer: KafkaConsumer,
    shutdown: asyncio.Future,
    should_exit: bool = False,
    poll_timeout: float = getattr(settings, "KAFKA_CONSUMER_TTL", 60),
    getone_timeout: Optional[float] = getattr(settings, "KAFKA_CONSUMER_GETONE_TIMEOUT", 60),
):
    running = True

    if should_exit:
        await asyncio.wait_for(consumer.wait_for_assignment(), poll_timeout)
        # Kafka position points to next record offset, e.g. current = offset - 1
        end_offsets = {
            tp: offset - 1 for tp, offset in
            (await asyncio.wait_for(consumer.end_offsets(consumer.assignment()), poll_timeout)).items()
        }
        logger.debug("Latest available offsets: %s", end_offsets)
        position = {tp: await asyncio.wait_for(consumer.position(tp), poll_timeout) - 1 for tp in consumer.assignment()}
        logger.debug("Committed offsets: %s", position)
        running = all(end_offsets[key] > offset for key, offset in position.items())
    else:
        end_offsets, position = {}, {}

    waiter = asyncio.create_task(consumer.getone())
    try:
        while running:
            done, _ = await asyncio.wait([waiter, shutdown], timeout=getone_timeout, return_when=asyncio.FIRST_COMPLETED)
            if done:
                if waiter.done():
                    msg: aiokafka.ConsumerRecord = waiter.result()
                    position[aiokafka.TopicPartition(msg.topic, msg.partition)] = msg.offset
                    await queues[msg.topic].put(msg)
                    waiter = asyncio.create_task(consumer.getone())
                    if should_exit:
                        running = all(end_offsets[key] > offset for key, offset in position.items())
                if shutdown.done():
                    running = False
            else:
                tps = consumer.assignment() - consumer.paused()
                for partition in tps:
                    poll_ts = consumer.last_poll_timestamp(partition) / 1000
                    if time.time() - poll_ts > poll_timeout:
                        raise ConsumerTimeoutError()
    finally:
        waiter.cancel()
    if not shutdown.done() or shutdown.exception() is None:
        logger.info("Prepare consumer exit")
        for queue in queues.values():
            await queue.put(None)
            await queue.join()
    logger.info("Consumer routine exit")


@dataclasses.dataclass
class KafkaImporterV2:
    topic: str
    msg_handler: Callable[[aiokafka.ConsumerRecord, Any], Any]
    timeout: int = getattr(settings, "KAFKA_BATCH_WAIT_TIME", 10)
    commit_timeout: int = getattr(settings, "KAFKA_COMMIT_TIMEOUT", 60)
    value_deserializer: Optional[Callable[[bytes], Any]] = getattr(settings, "KAFKA_SAFE_JSON_VALUE_DESERIALIZER", None)
    key_deserializer: Optional[Callable[[bytes], Any]] = getattr(settings, "KAFKA_DUMB_KEY_DESERIALIZER", None)
    context_provider: Optional[Callable[[], Any]] = None
    batch_size: int = DEFAULT_BATCH_SIZE
    batch_insert_func: Callable[[Type, List[Any], int], Any] = dataclasses.field(default_factory=lambda: _bulk_create)
    klass: Optional[Type] = None
    manual_commit: bool = getattr(settings, "KAFKA_MANUAL_COMMIT", True)
    close_db_connection: bool = getattr(settings, "KAFKA_CLOSE_DB_CONNECTION", True)
    close_db_in_routine: bool = False
    report_alive: int = getattr(settings, "KAFKA_REPORT_ALIVE", 0)
    bind_self: bool = False
    separate_executor: bool = False

    consumer: Optional[KafkaConsumer] = None
    queue: Optional[asyncio.Queue] = None

    def __post_init__(self):
        self.executor = None
        if self.separate_executor:
            self.executor = ThreadPoolExecutor(max_workers=1)

        if isinstance(self.key_deserializer, str):
            self.key_deserializer = import_string(self.key_deserializer)
        if isinstance(self.value_deserializer, str):
            self.value_deserializer = import_string(self.value_deserializer)

        # Django ORM sync, so msg_handler and batch_insert_func should be sync too
        if not asyncio.iscoroutinefunction(self.msg_handler):
            self.msg_handler = self.sync_to_async(self.msg_handler)
        elif isinstance(self.msg_handler, SyncToAsync):
            self.msg_handler = self.sync_to_async(self.msg_handler.func)
        else:
            self.close_db_in_routine = True

        if not asyncio.iscoroutinefunction(self.batch_insert_func):
            self.batch_insert_func = self.sync_to_async(self.batch_insert_func)
        elif isinstance(self.batch_insert_func, SyncToAsync):
            self.batch_insert_func = self.sync_to_async(self.batch_insert_func.func)
        else:
            self.close_db_in_routine = True

        self._close_db_in_routine = self.close_db_in_routine and (
            django.VERSION < (3, 0) or os.environ.get("DJANGO_ALLOW_ASYNC_UNSAFE")
        )

    def sync_to_async(self, func):
        if self.separate_executor:
            return SyncToAsync(func, thread_sensitive=False, executor=self.executor)
        else:
            return SyncToAsync(func)

    async def _call_func(self, func, *args, **kwargs):
        if self.bind_self:
            return await func(self, *args, **kwargs)
        else:
            return await func(*args, **kwargs)

    async def import_routine(self):
        last_report = 0
        db_maybe_connected = False

        ctx = {}
        if self.context_provider is not None:
            ctx = await self._call_func(self.context_provider)
            db_maybe_connected = True

        batch = []
        offsets = {}
        total_count = fail_count = pending = 0
        get_msg_task = asyncio.create_task(self.queue.get())

        while True:
            if self.report_alive:
                now = time.monotonic()
                if now - last_report > self.report_alive:
                    logger.info("Importer routine '%s' is alive", self.topic)
                    last_report = now

            done, _ = await asyncio.wait([get_msg_task], timeout=self.timeout)
            if done:
                msg = get_msg_task.result()
                try:
                    if msg is None:
                        await self._handle_batch(batch, offsets)
                        break
                    else:
                        offsets[msg.partition] = msg.offset
                        total_count += 1
                        if self.value_deserializer is not None:
                            msg.value = self.value_deserializer(msg.value)
                        if self.key_deserializer is not None:
                            msg.key = self.key_deserializer(msg.key)
                        obj = await self._call_func(self.msg_handler, msg, ctx)
                        db_maybe_connected = True
                        if obj is None or obj is HandlerResult.Fail:
                            fail_count += 1
                        elif obj is HandlerResult.Saved:
                            pending += 1
                        else:
                            pending += 1
                            batch.append(obj)
                            if pending >= self.batch_size:
                                await self._handle_batch(batch, offsets)
                                pending = 0
                                batch.clear()
                                offsets.clear()
                finally:
                    self.queue.task_done()

                get_msg_task = asyncio.create_task(self.queue.get())
                if total_count % LOG_EVERY_RECORD == 0:
                    logger.info(
                        f"Topic \033[32m'{self.topic}'\033[0m, processed "
                        f"\033[32m{total_count}\033[0m, fail count \033[31m{fail_count}\033[0m"
                    )
            else:
                if batch:
                    await self._handle_batch(batch, offsets)
                    pending = 0
                    batch.clear()
                    offsets.clear()
                if self.close_db_connection and db_maybe_connected:
                    logger.debug("Close db connections in sync_to_async")
                    await self.sync_to_async(db.connections.close_all)()
                    if self._close_db_in_routine:
                        logger.debug("Close db connections in routine")
                        db.connections.close_all()
                    db_maybe_connected = False
                    logger.debug("Done handling DB")
        logger.info(f"Processed for \033[32m{self.topic}\033[0m done. ✅️")

    async def _handle_batch(self, batch, offsets):
        if not batch:
            return
        cls = self.klass or type(batch[0])
        await self._call_func(self.batch_insert_func, cls, batch, self.batch_size)
        if self.manual_commit:
            to_commit = {
                aiokafka.TopicPartition(self.topic, partition): offset + 1 for partition, offset in offsets.items()
            }
            await asyncio.wait_for(self.consumer.commit(to_commit), self.commit_timeout)


async def main_v2(providers: List[Callable[[], KafkaImporterV2]], should_exit: bool = False, queue_size: int = 1):
    importers = [provider() for provider in providers]
    queues = {importer.topic: asyncio.Queue(queue_size) for importer in importers}
    consumer = KafkaConsumer(
        *queues,
        bootstrap_servers=settings.KAFKA_SERVERS,
        group_id=settings.KAFKA_GROUP_ID,
        auto_offset_reset=getattr(settings, "KAFKA_AUTO_OFFSET_RESET", "earliest"),
        enable_auto_commit=getattr(settings, "KAFKA_ENABLE_AUTO_COMMIT", False),
        request_timeout_ms=getattr(settings, "KAFKA_CONSUMER_TIMEOUT_MS", 40_000),
        retry_backoff_ms=getattr(settings, "KAFKA_RECONNECT_BACKOFF_MS", 500),
        **get_sasl_auth(),
    )
    shutdown = asyncio.Future()
    routines = [consumer_routine(queues, consumer, shutdown, should_exit=should_exit)]

    for importer in importers:
        importer.consumer = consumer
        importer.queue = queues[importer.topic]
        routines.append(importer.import_routine())
    return await _main(consumer, routines, shutdown)


async def _main(consumer, routines, shutdown):
    signals = (signal.SIGINT, signal.SIGTERM)
    loop = asyncio.get_event_loop()

    def graceful_exit():
        logger.info("Received exit signal")
        shutdown.set_result(None)
        for sig in signals:
            loop.remove_signal_handler(sig)

    for signum in signals:
        loop.add_signal_handler(signum, graceful_exit)

    try:
        await asyncio.wait_for(consumer.start(), timeout=getattr(settings, "KAFKA_CONSUMER_TIMEOUT_MS", 40_000) / 1000)
        tasks = list(map(asyncio.create_task, routines))
        done, pending = await asyncio.wait(tasks, return_when=asyncio.FIRST_EXCEPTION)
        if pending:
            for task in pending:
                task.cancel()
            await asyncio.wait(pending)
        for task in done:
            if task.exception() is not None:
                raise task.exception()
    finally:
        await asyncio.wait_for(consumer.stop(), timeout=getattr(settings, "KAFKA_STOP_TIMEOUT", 40))
    return shutdown.done()


@dataclasses.dataclass
class KafkaImporterConf:
    consumer: KafkaConsumer
    msg_handler: Coroutine[Any, Model]
    batch_size: int = 1000
    insert_func: Coroutine[tuple[Model, list[Model], int], None] = dataclasses.field(
        default_factory=lambda: _bulk_create
    )
    klass: Type[Model] = None
    context_provider: Optional[Callable[[], dict]] = None

    @staticmethod
    def consumer_factory(
        topics: str,
        bootstrap_servers=settings.KAFKA_SERVERS,
        auto_offset_reset=settings.KAFKA_AUTO_OFFSET_RESET,
        key_deserializer=settings.KAFKA_DUMB_KEY_DESERIALIZER,
        value_deserializer=settings.KAFKA_SAFE_JSON_VALUE_DESERIALIZER,
        group_id=settings.KAFKA_GROUP_ID,
        enable_auto_commit=settings.KAFKA_ENABLE_AUTO_COMMIT,
        request_timeout_ms=settings.KAFKA_CONSUMER_TIMEOUT_MS,
        retry_backoff_ms=settings.KAFKA_RECONNECT_BACKOFF_MS,
        auth_extra=get_sasl_auth(),
    ) -> KafkaConsumer:
        consumer = KafkaConsumer(
            topics,
            bootstrap_servers=bootstrap_servers,
            auto_offset_reset=auto_offset_reset,
            key_deserializer=key_deserializer,
            value_deserializer=value_deserializer,
            group_id=group_id,
            enable_auto_commit=enable_auto_commit,
            request_timeout_ms=request_timeout_ms,
            retry_backoff_ms=retry_backoff_ms,
            fetch_max_wait_ms=1000,
            **auth_extra,
        )
        return consumer


async def main(providers: List[Callable[[], KafkaImporterConf]], first_execution: bool = False, queue_size: int = 1):
    queues = {}
    configs = {}
    for provider in providers:
        config = provider()
        topic = next(iter(config.consumer.subscription()))
        configs[topic] = config
        queues[topic] = asyncio.Queue(queue_size)
        # Suppress unclosed consumer error
        await config.consumer.stop()

    consumer = KafkaConsumer(
        *queues,
        bootstrap_servers=settings.KAFKA_SERVERS,
        auto_offset_reset=settings.KAFKA_AUTO_OFFSET_RESET,
        group_id=settings.KAFKA_GROUP_ID,
        enable_auto_commit=settings.KAFKA_ENABLE_AUTO_COMMIT,
        request_timeout_ms=settings.KAFKA_CONSUMER_TIMEOUT_MS,
        retry_backoff_ms=settings.KAFKA_RECONNECT_BACKOFF_MS,
        **get_sasl_auth(),
    )
    shutdown = asyncio.Future()
    routines = [consumer_routine(queues, consumer, shutdown, should_exit=first_execution)]
    for topic, config in configs.items():
        importer = KafkaImporterV2(
            topic,
            msg_handler=config.msg_handler,
            timeout=settings.KAFKA_CONSUMER_TIMEOUT_MS / 1000,
            value_deserializer=config.consumer._value_deserializer,
            key_deserializer=config.consumer._key_deserializer,
            context_provider=config.context_provider,
            batch_size=config.batch_size,
            batch_insert_func=config.insert_func,
            klass=config.klass,
            consumer=consumer,
            queue=queues[topic],
        )
        routines.append(importer.import_routine())

    return await _main(consumer, routines, shutdown)
