import json
import os
import socket
from typing import List, Tuple

from django.conf import settings
from kafka import KafkaProducer

from django_kafka.auth import get_sasl_auth


def get_topic_name(feature_name, method, version=None):
    project_name = settings.PROJECT
    version = version if settings.STAND == 'PROD' else settings.STAND.lower()
    return f'{project_name}.{feature_name}.{method}.{version}'


michine_id = ''
try:
    michine_id = open('/etc/machine-id').read().rstrip()
except FileNotFoundError:
    pass


class KafkaProducerModify(KafkaProducer):
    """
    Modify send message with extra headers and bytes serializer for this headers.
    Other functionality the same
    """

    @staticmethod
    def _header_serializer(header_dict: dict) -> List[Tuple[str, bytes]]:
        res = []
        for _key, _value in header_dict.items():
            if isinstance(_value, (bytes, bytearray)):
                res.append((_key, _value))
            else:
                res.append((_key, _value.encode()))
        return res

    def send(self, *args, **kwargs):
        headers = kwargs.get('headers', {})
        headers.update(
            {
                'project': settings.PROJECT,
                'hostname': socket.gethostname(),
                'machine-id': michine_id,
            }
        )
        kwargs['headers'] = self._header_serializer(headers)
        return super(KafkaProducerModify, self).send(*args, **kwargs)


def get_producer(**kwargs):
    kafka_kwargs = dict(
        bootstrap_servers=settings.KAFKA_SERVERS,
        api_version=(0, 11, 5),
        key_serializer=lambda k: k.encode(),
        value_serializer=lambda m: json.dumps(m, ensure_ascii=False).encode(),
    )
    kafka_kwargs.update(get_sasl_auth())
    kafka_kwargs.update(kwargs)
    return KafkaProducerModify(**kafka_kwargs)
