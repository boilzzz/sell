import logging
import asyncio
from typing import Any
from importlib import import_module

from django.core.management import BaseCommand, CommandError
from django.core.management.base import CommandParser
from django.utils.module_loading import import_string
from django_kafka.kafka_consumer import main, main_v2


logger = logging.getLogger(__name__)


def coloring_text(txt, label):
    if label == "good":
        return f"\033[42m\033[30m{txt}\033[0m"


class Command(BaseCommand):
    PROVIDERS_OPT = "providers"
    RUNNER_OPT = "runner"
    APPLICATIONS_OPT = "apps"
    QUEUE_SIZE = "queue_size"

    def add_arguments(self, parser: CommandParser) -> None:
        parser.add_argument(
            "-r",
            "--runner",
            dest=self.RUNNER_OPT,
            type=int,
            default=1,
        )
        parser.add_argument(
            "-t",
            "--target",
            dest=self.PROVIDERS_OPT,
            action="append",
            nargs="+",
        )
        parser.add_argument(
            "-a",
            "--app",
            dest=self.APPLICATIONS_OPT,
            nargs="+",
            help="list of apps for consuming",
        )
        parser.add_argument(
            "-i",
            "--initial",
            dest="full_restore",
            action="store_true",
            help="start consuming after sync every topic",
        )
        parser.add_argument(
            "-q",
            "--queue-size",
            dest=self.QUEUE_SIZE,
            type=int,
            default=1,
            help="Consumer -> Importer msg queue size",
        )
        return super().add_arguments(parser)

    def handle(self, *args: Any, **options: Any) -> Any:
        if not any((options[self.APPLICATIONS_OPT], options[self.PROVIDERS_OPT])):
            raise CommandError(f"Specify {self.APPLICATIONS_OPT} or {self.PROVIDERS_OPT}")
        providers = []
        if apps := options[self.APPLICATIONS_OPT]:
            for app in apps:
                app_consumers = import_module(f"{app}.consumers")
                for _function_name, _function in app_consumers.__dict__.items():
                    if _function_name.endswith("_consumer"):
                        providers.append(_function)
        else:
            for provider in options[self.PROVIDERS_OPT]:
                providers += provider
            providers = list(map(import_string, providers))
        runner = main_v2 if options[self.RUNNER_OPT] == 2 else main
        queue_size = options[self.QUEUE_SIZE]
        if options["full_restore"]:
            for provider in providers:
                logger.info(coloring_text("==== Go to the next topic ====", "good"))
                # Runner returns True when got sigint
                if asyncio.run(runner([provider], True, queue_size=queue_size)):
                    return
        logger.info(coloring_text("==== START ALL LISTENERS ====", "good"))
        asyncio.run(runner(providers, queue_size=queue_size))
