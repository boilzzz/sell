from django.contrib import admin

# Register your models here.
from post.models import Post, Comment


class CommentInline(admin.StackedInline):
    model = Comment


@admin.register(Post)
class EventTemplateAdmin(admin.ModelAdmin):
    inlines = [CommentInline]


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_filter = ('approved',)
    list_display = ('post', 'text', 'approved')
