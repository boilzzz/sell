from typing import TypedDict


class PostExportDataStructure(TypedDict):
    topic: str
    text: str
