from __future__ import annotations

import logging
from typing import Any, List, Optional

from django.conf import settings
from django_kafka.kafka_consumer import KafkaImporterConf
from post.models import Comment

logger = logging.getLogger(__name__)


def parse_comment_message(msg: Any, *args, **kwargs) -> Optional[Comment]:
    """
    Parse messages from dux.tep.export
    """
    try:
        data = msg.value
        # guid field exists for 1c objects only
        logger.info(data)
        comment = Comment(
            id=data.get('id'),
            approved=data.get('approve'),
        )
        return comment

    except ValueError:
        logger.warning('got invalid msg key: "%s", or value:\n%s', msg.key, msg.value)
    except TypeError:
        pass


def bulk_create_comment(cls: Comment, objs: List[Comment], batch_size: int):
    """
    Insert function for Django-Kafka config
    """
    to_update = []

    for obj in objs:
        to_update.append(obj)

    cls.objects.bulk_update(to_update, ['approved'], batch_size=batch_size)

    logger.info('Update: %s.', len(to_update))


def comment_consumer() -> KafkaImporterConf:
    """
    Main function to get config and run consumer
    """
    topic = settings.KAFKA_COMMENT_APPROVED_TOPIC
    return KafkaImporterConf(
        consumer=KafkaImporterConf.consumer_factory(topic),
        msg_handler=parse_comment_message,
        klass=Comment,
        insert_func=bulk_create_comment,
    )
