from typing import List

from django.contrib.auth.models import User
from django.db import models


# Create your models here.
class ModelSerializerDict(models.Model):
    class Meta:
        abstract = True

    def as_dict(self, fields: List[str] = None):
        model_fields = self._meta.fields
        if fields is None:
            fields = [field.name for field in model_fields]
        dict = {field: getattr(self, field) for field in fields if
                field in fields}
        return dict


class Post(ModelSerializerDict):
    topic = models.CharField(verbose_name='Тема', max_length=8912)
    text = models.TextField(verbose_name='Текст поста')

    class Meta:
        verbose_name = 'пост'
        verbose_name_plural = 'посты'

    def __str__(self):
        return self.topic


class Comment(ModelSerializerDict):
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='comments')
    text = models.TextField(verbose_name='Текст комментария')
    author = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='автор')
    approved = models.BooleanField(verbose_name='Подтвержден', default=False)

    class Meta:
        verbose_name = 'комментарий'
        verbose_name_plural = 'комментарии'


