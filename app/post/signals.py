import logging
from django.db.models.signals import post_save
from django.dispatch import receiver
from post.models import Post, Comment
from sell.kafka import kafka_export

logger = logging.getLogger(__name__)


@receiver(post_save, sender=Post)
def export_post_data_to_kafka_on_save(sender, instance, created, **kwargs, ):
    kafka_export.delay('post', instance.as_dict(), instance.id)


@receiver(post_save, sender=Comment)
def export_comment_data_to_kafka_on_save(sender, instance, created, **kwargs, ):
    kafka_export.delay('comment', instance.as_dict(['id', 'text']), instance.id)
