from django.db.models import Prefetch
from django.shortcuts import render

# Create your views here.
from django.views.decorators.csrf import csrf_exempt

from post.models import Post, Comment


@csrf_exempt
def get_post(request):
    context = {'posts': Post.objects.all().prefetch_related(
        Prefetch('comments', queryset=Comment.objects.filter(approved=True)))}
    return render(request, 'index.html', context)
