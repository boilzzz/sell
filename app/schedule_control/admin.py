from django.contrib import admin

# Register your models here."
from schedule_control.models import CommentApprove


@admin.register(CommentApprove)
class CommentApproveAdmin(admin.ModelAdmin):
    list_display = ('id', 'text', 'approve')
    readonly_fields = ('text', )
