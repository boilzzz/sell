from django.apps import AppConfig


class ScheduleControlConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'schedule_control'

    def ready(self):
        import schedule_control.signals

