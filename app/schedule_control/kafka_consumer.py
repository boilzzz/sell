from __future__ import annotations

import logging
from typing import Any, List, Optional

from django.conf import settings
from django_kafka.kafka_consumer import KafkaImporterConf
from schedule_control.models import CommentApprove

logger = logging.getLogger(__name__)


def parse_comment_message(msg: Any, *args, **kwargs) -> Optional[CommentApprove]:
    """
    Parse messages from dux.tep.export
    """
    try:
        data = msg.value
        # guid field exists for 1c objects only
        logger.info(data)
        comment = CommentApprove(
            id=data.get('id'),
            text=data.get('text'),
            approve=False
        )
        return comment

    except ValueError:
        logger.warning('got invalid msg key: "%s", or value:\n%s', msg.key, msg.value)
    except TypeError:
        raise


def bulk_create_comment(cls: CommentApprove, objs: List[CommentApprove], batch_size: int):
    """
    Insert function for Django-Kafka config
    """
    to_create = []

    exists_id = cls.objects.values_list('id', flat=True)
    for obj in objs:
        if obj.id not in exists_id:
            to_create.append(obj)

    cls.objects.bulk_create(to_create, batch_size=batch_size, ignore_conflicts=True)
    logger.info(to_create)
    logger.info('Created: %s.', len(to_create))


def comment_approved_consumer() -> KafkaImporterConf:
    """
    Main function to get config and run consumer
    """
    topic = settings.KAFKA_COMMENT_TOPIC
    return KafkaImporterConf(
        consumer=KafkaImporterConf.consumer_factory(topic),
        msg_handler=parse_comment_message,
        klass=CommentApprove,
        insert_func=bulk_create_comment,
    )
