from typing import List

from django.db import models


# Create your models here.

class ModelSerializerDict(models.Model):
    class Meta:
        abstract = True

    def as_dict(self, fields: List[str] = None):
        model_fields = self._meta.fields
        if fields is None:
            fields = [field.name for field in model_fields]
        dict = {field: getattr(self, field) for field in fields if
                field in fields}
        return dict


class CommentApprove(ModelSerializerDict):
    text = models.TextField(verbose_name='Текст комментария')
    approve = models.BooleanField(verbose_name='Подтвердить', default=None)

    class Meta:
        verbose_name = 'Подтверждение комментария'
        verbose_name_plural = 'Подтверждение комментариев'

    def __str__(self):
        return f"{self.id}"
