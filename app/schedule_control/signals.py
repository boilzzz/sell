from django.db.models.signals import post_save
from django.dispatch import receiver

from schedule_control.models import CommentApprove
from sell.kafka import kafka_export


@receiver(post_save, sender=CommentApprove)
def export__approved_comment_data_to_kafka_on_save(sender, instance: CommentApprove, created,
                                                   **kwargs, ):
    kafka_export.delay('comment-approved', instance.as_dict(['id', 'approve']), instance.id)
