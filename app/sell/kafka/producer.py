from typing import Union
from celery import shared_task
from django_kafka.kafka_producer import get_topic_name, get_producer


@shared_task
def kafka_export(
        feature_name: str, data: dict, id: Union[int, str], method: str = 'export', version: int = 1
):
    topic_name = get_topic_name(feature_name, method, version)
    producer = get_producer()
    producer.send(topic_name, key=str(id), value=data)
    producer.flush()
    producer.close()
